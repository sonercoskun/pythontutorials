#!/usr/bin/env python

# import modules used here -- sys is a very standard one
import sys

# Gather our code in a main() function
def merhaba(isim,yas):
    print 'merhaba' , isim , str(yas) , 'yasindasin'
    if 'ali' == isim or yas == 35 :
        print 'seni taniyorum'
    else:
        print 'seni tanimiyorum'
def main():
    merhaba('ali',30)
    # Command line args are in sys.argv[1], sys.argv[2] ...
    # sys.argv[0] is the script name itself and can be ignored

# Standard boilerplate to call the main() function to begin
# the program.
if __name__ == '__main__':
    main()